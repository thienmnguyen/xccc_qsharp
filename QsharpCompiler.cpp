#include "QsharpCompiler.hpp"
#include "xacc_service.hpp"

ITreeWalker* g_qsAstTreeWalker  = nullptr;

namespace xacc {
QsharpCompiler::QsharpCompiler():
    m_syntaxListener(),
    m_errorListener(),
    m_host(m_syntaxListener, m_errorListener),
    m_hostInitialized(false)
{    
    std::string rootPath = QSHARP_ADAPTER_DLL_PATH;
    // If the path is missing the dir separator, add one.
    if (rootPath.back() != '/')
    {
        rootPath += '/';
    }

    if (!m_host.Initialize(rootPath))
    {
        std::cout << "Failed to initialize Qsharp compiler host!!\n";
    }
    else
    {
       m_hostInitialized = true;
       g_qsAstTreeWalker = this;
    }    
}

std::shared_ptr<xacc::IR> QsharpCompiler::compile(const std::string& src, std::shared_ptr<Accelerator> acc) 
{
    if (!m_hostInitialized)
    {
        return nullptr;
    }
    
    m_irProvider = xacc::getService<xacc::IRProvider>("quantum");
    m_compiledIr = m_irProvider->createIR();
    m_qubitNameToIdx.clear();
    m_qubitIdx = 0;
    m_host.Compile(src);
    

    return m_compiledIr; 
}

std::shared_ptr<xacc::IR> QsharpCompiler::compile(const std::string& src) 
{
    return nullptr;
    //TODO
}

const std::string QsharpCompiler::translate(std::shared_ptr<CompositeInstruction> function) 
{
    return "";
    // TODO
}

void QsharpCompiler::enterGate(const std::string& in_gateName, const std::vector<std::string>& in_qubits, const std::vector<GateVariable>& in_gateVars) 
{
    std::vector<size_t> bits;

    for (const auto& qbit : in_qubits)
    {
        const auto iter = m_qubitNameToIdx.find(qbit);
        if (iter != m_qubitNameToIdx.end())
        {
            bits.emplace_back(iter->second);
        }
        else
        {
            m_qubitNameToIdx.emplace(qbit, m_qubitIdx);
            bits.emplace_back(m_qubitIdx);
            m_qubitIdx++;
        }
    }

    std::vector<InstructionParameter> params;

    for (const auto& param : in_gateVars)
    {
        if(param.isNamedVariable())
        {
            const std::string paramName = param.getName().value();
            params.emplace_back(paramName);
        }
        else
        {
            params.emplace_back(param.getValue().value());
        }
    }
    
    const auto qsGateNameToXaccGateName = [](const std::string& in_qsGateName) -> std::string {
        if (in_qsGateName == "M")
        {
            return "Measure";
        }
        if (in_qsGateName == "SWAP")
        {
            return "Swap";
        }
        
        return in_qsGateName;
    };

    auto inst = m_irProvider->createInstruction(qsGateNameToXaccGateName(in_gateName), bits, params);
    m_currentKernel->addInstruction(inst);
}

void QsharpCompiler::enterKernel(const std::string& in_kernelName, const std::vector<KernelVariable>& in_vars) 
{
    m_currentKernel = m_irProvider->createComposite(in_kernelName);
    m_qubitNameToIdx.clear();
    m_qubitIdx = 0;

    m_compiledIr->addComposite(m_currentKernel);
    for (const auto& var : in_vars)
    {
        if (var.m_type.find("Qubit") == std::string::npos)
        {
            m_currentKernel->addVariable(var.m_name);
        }
    }
}
}