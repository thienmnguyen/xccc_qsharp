#include "cppmicroservices/BundleActivator.h"
#include "cppmicroservices/BundleContext.h"
#include "cppmicroservices/ServiceProperties.h"
#include <iostream>
#include "QsharpCompiler.hpp"

using namespace cppmicroservices;

namespace {
class US_ABI_LOCAL QsharpCompilerActivator : public BundleActivator 
{

public:
    QsharpCompilerActivator() {}

    void Start(BundleContext context) 
    {
        auto qsCompiler = std::make_shared<xacc::QsharpCompiler>();
        context.RegisterService<xacc::Compiler>(qsCompiler);
    }

    void Stop(BundleContext context) {}
};

} // namespace

CPPMICROSERVICES_EXPORT_BUNDLE_ACTIVATOR(QsharpCompilerActivator)