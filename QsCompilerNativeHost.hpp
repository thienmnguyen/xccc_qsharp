#pragma once

#include "QsABI/QsABI.h"
#include <string>
#include "coreclr_delegates.h"

namespace xacc {
namespace Interop{
class QsCompilerHost
{
public:
    QsCompilerHost(SyntaxListener& io_syntaxListener, ErrorListener& io_errorListenter);
    bool Initialize(const std::string& in_dllRootPath);
    bool Compile(const std::string& in_srcString);
private:
    struct QsCompileArgs
    {
        const char_t* src;
        void* syntaxListener;
        void* errorListener;
    };

    struct QsNodeQueryArgs
    {
        int nodeIs;
        void* reserved;
    };

    SyntaxListener& m_syntaxListener;
    ErrorListener& m_errorListener;
    bool m_isInitialized;
    component_entry_point_fn m_compileFnPtr;
    component_entry_point_fn m_nodeInfoQueryFnPtr;
    component_entry_point_fn m_visitNodeFnPtr;
};
}
} 