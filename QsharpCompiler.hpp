#pragma once

#include "Compiler.hpp"
#include "QsCompilerNativeHost.hpp"
#include "QsharpHostConfig.h"
#include "IRProvider.hpp"

namespace xacc {

class QsharpCompiler : public xacc::Compiler, public ITreeWalker
{
public:
    QsharpCompiler();

    std::shared_ptr<xacc::IR> compile(const std::string& src, std::shared_ptr<Accelerator> acc) override;

    std::shared_ptr<xacc::IR> compile(const std::string& src) override;

    const std::string translate(std::shared_ptr<CompositeInstruction> function) override;

    const std::string name() const override { return "qsharp"; }

    const std::string description() const override { return ""; }

    virtual ~QsharpCompiler() {}

    void enterGate(const std::string& in_gateName, const std::vector<std::string>& in_qubits, const std::vector<GateVariable>& in_gateVars) override;
    void enterKernel(const std::string& in_kernelName, const std::vector<KernelVariable>& in_vars) override;
private:
    SyntaxListener m_syntaxListener;
    ErrorListener m_errorListener;
    xacc::Interop::QsCompilerHost m_host;
    bool m_hostInitialized;
    std::shared_ptr<xacc::IRProvider> m_irProvider;
    std::shared_ptr<xacc::IR> m_compiledIr;
    std::shared_ptr<xacc::CompositeInstruction> m_currentKernel;
    std::unordered_map<std::string, size_t> m_qubitNameToIdx;
    size_t m_qubitIdx;
};
}