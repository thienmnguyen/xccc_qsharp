﻿using System;
using System.IO;
using System.Text;
using System.Runtime.InteropServices;
using System.Collections.Immutable;
using Microsoft.Quantum.QsCompiler;
using Microsoft.Quantum.QsCompiler.CompilationBuilder;
using Microsoft.Quantum.QsCompiler.DataTypes;
using Microsoft.Quantum.QsCompiler.SyntaxTree;
using Microsoft.Quantum.QsCompiler.Transformations.BasicTransformations;
using System.Collections.Generic;
using Microsoft.Quantum.QsCompiler.SyntaxTokens; 
using System.Linq;
using XACC;

namespace XACC_Adapter
{
    [StructLayout(LayoutKind.Sequential)]
    public class QsNamespaceNode
    {
        [MarshalAs(UnmanagedType.LPStr)]
        public string name;
    }

    [StructLayout(LayoutKind.Sequential)]
    public class QsCompilationNode
    {
        public int numNamespaces;
        public IntPtr namespacesArrPtr;
    }

    public enum QS_AST_NODE_TYPE
    {
        QS_INVALID,
        QS_STATEMENT,
        QS_EXPRESSION
    };

    public enum QS_AST_VISIT_RESULT
    {
        VISIT_BREAK,
        VISIT_CONTINUE,
        VISIT_RECURSE
    };

    [StructLayout(LayoutKind.Sequential)]
    public class QsKernelVar
    {
        [MarshalAs(UnmanagedType.LPStr)]
        public string name;
        [MarshalAs(UnmanagedType.LPStr)]
        public string type;
    }

    [StructLayout(LayoutKind.Sequential)]
    public class QsParametricVar
    {
        public bool isVar;
        [MarshalAs(UnmanagedType.LPStr)]
        public string varName;
        public double value;
    }

    public static class CompilerAdapter
    {
        private static string TEMP_FILE_NAME = "_XACC_TEMP_QSHARP_SRC.qs";

        [DllImport(@"libQsABI.so")]
        public static extern void OnCompilationCompleted(IntPtr listener, QsCompilationNode compilationResult);

        [DllImport(@"libQsABI.so")]
        public static extern void OnIntrinsicGateVisited(IntPtr listener, string gateName, int arity, string q1, string q2, int nbVars, IntPtr paramArray);

        [DllImport(@"libQsABI.so")]
        public static extern void OnKernelVisited(IntPtr listener, string kernelName, int nbVars, IntPtr varArray);


        [DllImport(@"libQsABI.so")]
        public static extern QS_AST_VISIT_RESULT VisitNode(int nodeId, QS_AST_NODE_TYPE nodeType, int parentNodeId, IntPtr clientData);



        [StructLayout(LayoutKind.Sequential)]
        public struct XACC_Adapter_Args
        {
            // Source string
            public IntPtr SourceFile;
            // Listener (AST tree walk)
            public IntPtr ListenerPtr;
            // Error listener
            public IntPtr ErrorListenerObj;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct Node_Visit_Args
        {
            public int NodeId;
            public IntPtr ClientData;
        }

        private static int nodeIdCounter = 0;
        private static Dictionary<int, QsStatementKind> statementNodes = new Dictionary<int, QsStatementKind>();
        private static Dictionary<int, QsExpressionKind<TypedExpression, Identifier, ResolvedType>> expressionNodes = new Dictionary<int, QsExpressionKind<TypedExpression, Identifier, ResolvedType>>();

        public static int Compile(IntPtr arg, int argLength)
        {
            if (argLength < System.Runtime.InteropServices.Marshal.SizeOf(typeof(XACC_Adapter_Args)))
            {
                return 1;
            }

            XACC_Adapter_Args libArgs = Marshal.PtrToStructure<XACC_Adapter_Args>(arg);
            string fileContent = RuntimeInformation.IsOSPlatform(OSPlatform.Windows)
                ? Marshal.PtrToStringUni(libArgs.SourceFile)
                : Marshal.PtrToStringUTF8(libArgs.SourceFile);

            var compilationManager = new CompilationUnitManager();
            var file = new FileContentManager(new Uri(Path.GetFullPath(TEMP_FILE_NAME)), NonNullable<string>.New(TEMP_FILE_NAME));

            file.ReplaceFileContent(fileContent);

            //Console.WriteLine("-- The source file contains {0} lines. \n", file.NrLines());
            //Console.Write("-- Source File:\n{0}\n", fileContent);

            compilationManager.AddOrUpdateSourceFilesAsync(ImmutableHashSet.Create(file));
            var homePath = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
            // Intrinsic gates: X, Y, Z, H, CNOT, etc.
            var qsharpIntrinsicLib = homePath + "/.xacc/lib/Microsoft.Quantum.QSharp.Core.dll";
            var qsharpStandardLib = homePath + "/.xacc/lib/Microsoft.Quantum.Standard.dll";

            IEnumerable<string> refs = new List<string>() {
                qsharpIntrinsicLib,
                qsharpStandardLib
            };

            var headers = ProjectManager.LoadReferencedAssemblies(refs);
            var references = new References(headers, (code, args) => { });
            foreach (var key in references.Declarations.Keys)
            {
                Console.Write("-- Loaded DLL:{0}\n", key.Value);
            }
            compilationManager.UpdateReferencesAsync(references);

            var syntaxTree = compilationManager.GetSyntaxTree()
                            .Where(ns =>
                            {
                                // We only want those namespace nodes from the temp source file,
                                // i.e. the source string from the host.
                                // Hence, skip all the reference namespaces, e.g. those from DLL's.
                                return !GetSourceFiles.Apply(new QsNamespace[] { ns })
                                                     .Where(refPath => !refPath.Value.EndsWith(TEMP_FILE_NAME))
                                                     .Any();
                            });

            var compilationResult = new QsCompilationNode()
            {
                numNamespaces = syntaxTree.ToImmutableArray().Length,
                namespacesArrPtr = Marshal.AllocHGlobal(syntaxTree.ToImmutableArray().Length * Marshal.SizeOf(typeof(QsNamespaceNode)))
            };

            QsNamespaceNode[] namespaces = new QsNamespaceNode[syntaxTree.ToImmutableArray().Length];

            IntPtr current = compilationResult.namespacesArrPtr;
            for (int i = 0; i < syntaxTree.ToImmutableArray().Length; i++)
            {
                StringBuilder sb = new StringBuilder(syntaxTree.ToImmutableArray()[i].Name.Value);
                // Create a new struct and copy the unmanaged one to it
                namespaces[i] = new QsNamespaceNode()
                {
                    name = sb.ToString()
                };

                Marshal.StructureToPtr(namespaces[i], current, false);
                current = (IntPtr)((long)current + Marshal.SizeOf(namespaces[i]));
            }

            var globals = compilationManager.GetSyntaxTree().GlobalCallableResolutions();

            foreach (var ns in syntaxTree)
            {
                Console.WriteLine("Enter namespace {0}.\n", ns.Name);
                //Console.WriteLine("Data: {0}.\n", ns);

                foreach (var element in ns.Elements)
                {
                    if (element is QsNamespaceElement.QsCallable callableDecl)
                    {
                        // Console.WriteLine("QsCallable: '{0}'\n", callableDecl.GetFullName());
                        var callable = callableDecl.Item;
                        var definedVars = SyntaxGenerator.ExtractItems(callable.ArgumentTuple);

                        var varArray = new List<QsKernelVar>();
                        foreach (var callableVar in definedVars)
                        {
                            if (callableVar.VariableName is QsLocalSymbol.ValidName argName)
                            {
                                // Console.WriteLine(" - Arg name: '{0}'; Type = {1} \n", argName.Item.Value, callableVar.Type.Resolution);
                                var newVar = new QsKernelVar()
                                {
                                    name = argName.Item.Value.ToString(),
                                    type = callableVar.Type.Resolution.ToString()
                                };

                                varArray.Add(newVar);
                            }
                        }

                        // Kernel:
                        QsKernelVar[] data = varArray.ToArray();
                        var pData = Marshal.AllocHGlobal(data.Length * Marshal.SizeOf(typeof(QsKernelVar)));
                        int size = Marshal.SizeOf(typeof(QsKernelVar));
                        IntPtr mem = pData;
                        for (int ix = 0; ix < data.Length; ++ix)
                        {
                            Marshal.StructureToPtr(data[ix], mem, false);
                            mem = new IntPtr((long)mem + size);
                        }

                        OnKernelVisited(libArgs.ListenerPtr, callableDecl.GetFullName().ToString(), data.Length, pData);
                        Marshal.FreeHGlobal(pData);

                        var bodyDecl = callable.Specializations
                                        .Where(spec => spec.Kind == QsSpecializationKind.QsBody)
                                        .Single()
                                        .Implementation;

                        // The body is provided
                        if (bodyDecl is SpecializationImplementation.Provided bodyContent)
                        {
                            var statements = bodyContent.Item2.Statements;

                            foreach (var statement in statements)
                            {
                                statementNodes.Add(nodeIdCounter, statement.Statement);
                                nodeIdCounter++;

                                processStatement(statement, libArgs.ListenerPtr, globals);
                            }
                        }
                        else
                        {
                            Console.WriteLine("Implementation Not Provided: {0}.\n", bodyDecl);
                        }
                    }
                }
            }

            // Notify the listener
            OnCompilationCompleted(libArgs.ListenerPtr, compilationResult);

            return 0;
        }

        public static int GetNodeType(IntPtr arg, int argLength)
        {
            if (argLength < System.Runtime.InteropServices.Marshal.SizeOf(typeof(Node_Visit_Args)))
            {
                return 1;
            }

            Node_Visit_Args libArgs = Marshal.PtrToStructure<Node_Visit_Args>(arg);

            var nodeId = libArgs.NodeId;

            // TODO
            //Console.Write("Retrieving node type for node Id {0}\n", nodeId);
            return (int)QS_AST_NODE_TYPE.QS_INVALID;
        }

        public static int VisitChildren(IntPtr arg, int argLength)
        {
            //Console.Write("VisitChildren called!\n");

            return 0;
        }

        private static void EnterExpresstion(QsExpressionKind<TypedExpression, Identifier, ResolvedType> expression, IntPtr listener, ImmutableDictionary<QsQualifiedName, QsCallable> globalCallables)
        {
            expressionNodes.Add(nodeIdCounter, expression);
            nodeIdCounter++;

            var parsedExpression = SyntaxTreeParser.fromExpression(expression);


            if (parsedExpression.Value.gate.StartsWith("Global."))
            {
                var callableName = parsedExpression.Value.gate.Substring(7);

                foreach (var entry in globalCallables)
                {
                    if (entry.Key.Name.Value == callableName)
                    {
                        Console.WriteLine("Global callable {0}.\n", entry.Key.Name.Value);
                        var bodyDecl = entry.Value.Specializations
                                                .Where(spec => spec.Kind == QsSpecializationKind.QsBody)
                                                .Single()
                                                .Implementation;
                        
                        SyntaxTreeParser.resolveLibCallExpression(bodyDecl, expression);

                        if (bodyDecl is SpecializationImplementation.Provided bodyContent)
                        {
                            var statements = bodyContent.Item2.Statements;

                            foreach (var statement in statements)
                            {
                                processStatement(statement, listener, globalCallables);
                            }
                        }
                        else
                        {
                            Console.WriteLine("Implementation Not Provided: {0}.\n", bodyDecl);
                        }
                    }
                }
            }
            else
            {
                var paramArray = new List<QsParametricVar>();

                foreach (var param in parsedExpression.Value.gateParams)
                {
                    var qsParam = new QsParametricVar
                    {
                        isVar = String.Equals(param.typeName, "string"),
                        varName = String.Equals(param.typeName, "string") ? param.value : "",
                        value = String.Equals(param.typeName, "string") ? 0.0 : Convert.ToDouble(param.value)
                    };

                    paramArray.Add(qsParam);
                }
                var paramData = paramArray.ToArray();
                var pParamData = Marshal.AllocHGlobal(paramData.Length * Marshal.SizeOf(typeof(QsParametricVar)));
                int size = Marshal.SizeOf(typeof(QsParametricVar));
                IntPtr mem = pParamData;
                for (int ix = 0; ix < paramData.Length; ++ix)
                {
                    Marshal.StructureToPtr(paramData[ix], mem, false);
                    mem = new IntPtr((long)mem + size);
                }

                if (parsedExpression.Value.qubits.Length == 1)
                {
                    OnIntrinsicGateVisited(listener, parsedExpression.Value.gate, 1, parsedExpression.Value.qubits[0], "", paramData.Length, pParamData);
                }
                else if (parsedExpression.Value.qubits.Length == 2)
                {
                    OnIntrinsicGateVisited(listener, parsedExpression.Value.gate, 2, parsedExpression.Value.qubits[0], parsedExpression.Value.qubits[1], paramData.Length, pParamData);
                }
                else
                {
                    throw new InvalidOperationException("Error: " + expression.ToString());
                }
            }
        }

        private static void processStatement(QsStatement in_statement, IntPtr listener, ImmutableDictionary<QsQualifiedName, QsCallable> globalCallables)
        {
            switch (in_statement.Statement)
            {
                case QsStatementKind.QsExpressionStatement exprStm:
                    EnterExpresstion(exprStm.Item.Expression, listener, globalCallables);
                    break;
                case QsStatementKind.QsReturnStatement retStm:
                    Console.WriteLine("QsReturnStatement: {0}.\n", retStm);
                    break;
                case QsStatementKind.QsFailStatement failStm:
                    break;
                case QsStatementKind.QsVariableDeclaration varDeclStm:
                    Console.WriteLine("QsVariableDeclaration: {0}.\n", varDeclStm);
                    break;
                case QsStatementKind.QsValueUpdate valUpdateStm:
                    Console.WriteLine("QsValueUpdate: {0}.\n", valUpdateStm);
                    break;
                case QsStatementKind.QsConditionalStatement condStm:
                    Console.WriteLine("QsConditionalStatement: {0}.\n", condStm);
                    break;
                case QsStatementKind.QsForStatement forStm:
                    var forStmt = SyntaxTreeParser.parseForExpression(in_statement);
                    Console.WriteLine("QsForStatement: {0}.\n", forStmt.Value);
                    break;
                case QsStatementKind.QsWhileStatement whileStm:
                    Console.WriteLine("QsWhileStatement: {0}.\n", whileStm);
                    break;
                case QsStatementKind.QsRepeatStatement repeatStm:
                    Console.WriteLine("QsRepeatStatement: {0}.\n", repeatStm);
                    break;
                case QsStatementKind.QsConjugation conjStm:
                    Console.WriteLine("QsConjugation: {0}.\n", conjStm);
                    break;
                case QsStatementKind.QsQubitScope qbScopeStm:
                    var parsedData = SyntaxTreeParser.parseQubitScopeBlock(qbScopeStm.Item);
                    Console.WriteLine("Item1: {0}.\nItem2: {1}.\nItem3: {2}.\n", parsedData.Item1, parsedData.Item2, parsedData.Item3);
                    
                    foreach (var statement in qbScopeStm.Item.Body.Statements)
                    {
                        processStatement(statement, listener, globalCallables);
                    }
                    break;
                default:
                    throw new InvalidOperationException("unknown statement type");
            }
        }

    }
}
