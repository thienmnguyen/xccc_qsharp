﻿module XACC.SyntaxTreeParser

open System
open System.Collections.Immutable
open Microsoft.Quantum.QsCompiler.DataTypes
open Microsoft.Quantum.QsCompiler.SyntaxTokens
open Microsoft.Quantum.QsCompiler.SyntaxTree


type GateParam = {
    typeName:   string
    value:      string
}

/// A call to a quantum gate, with the given functors and arguments
type GateCall = {
    gate:       string
    qubits:     List<string>
    gateParams: List<GateParam>
}

/// Model XACC 'for' statement (very limited) 
type ForStatement = {
    beginIdx:       int
    endIdx:         int
    comparator:     string
    varName:        string
    inc_or_dec:     string
    // Initially, we only support simple 1-level for loops
    body:           List<GateCall>
}

/// Returns the gate name as a string
let rec getGateName (expr: QsExpressionKind<TypedExpression, Identifier, ResolvedType>) =
    let isSelfAdjoint (typeKind:  QsTypeKind<ResolvedType, UserDefinedType, QsTypeParameter, CallableInformation>) = 
        match typeKind with
        | QsTypeKind.Operation (_, opCharacteristics) -> if opCharacteristics.InferredInformation.IsSelfAdjoint then true else false
        | _ -> false

    let getAdjointGateName (expr: QsExpressionKind<TypedExpression, Identifier, ResolvedType>) =
        String.Concat(getGateName expr, "dg")

    let isIntrinsic(callable: QsQualifiedName) = 
        if callable.Namespace.Value.Contains("Intrinsic") then true else false

    match expr with
    | Identifier (GlobalCallable qualName, _) ->  if isIntrinsic qualName then qualName.Name.Value else String.Concat("Global.", qualName.Name.Value)
    | AdjointApplication (origExpr) -> if isSelfAdjoint origExpr.ResolvedType.Resolution then getGateName origExpr.Expression else getAdjointGateName origExpr.Expression
    | ControlledApplication (origExpr) -> String.Concat("C", getGateName origExpr.Expression)
    | _ -> printfn "Unknown %O"  expr; "UNKNOWN"


let rec containsQubit (typeKind:  QsTypeKind<ResolvedType, UserDefinedType, QsTypeParameter, CallableInformation>) = 
    match typeKind with
    | QsTypeKind.Qubit -> true
    | QsTypeKind.ArrayType t -> containsQubit t.Resolution
    | _ -> false

/// Returns the qubit list as an array of strings
let getQubitVars (expr: QsExpressionKind<TypedExpression, Identifier, ResolvedType>) =
    let getQregName (qRegExpr: QsExpressionKind<TypedExpression, Identifier, ResolvedType>) = 
        match qRegExpr with
        | Identifier (LocalVariable varName, _) ->  varName.Value
        | _ -> "UNKNOWN"
    let getQregIdx (qRegIdx: QsExpressionKind<TypedExpression, Identifier, ResolvedType>) = 
        match qRegIdx with
        | IntLiteral (idx) ->  idx
        | _ -> int64(-1)

    let constructQubitName (varExpr: QsExpressionKind<TypedExpression, Identifier, ResolvedType>) = 
        match varExpr with
        | Identifier (LocalVariable varName, _) ->  varName.Value
        | ArrayItem (qReg, idx) -> String.Concat(getQregName qReg.Expression, "[", getQregIdx idx.Expression, "]")
        | _ -> "UNKNOWN"

   

    match expr with
    | Identifier (_, _) ->  [constructQubitName  expr]
    | ArrayItem (_, _) -> [constructQubitName expr]
    | ValueTuple (varTuple) -> 
        varTuple |> Seq.map (function var -> if containsQubit var.ResolvedType.Resolution then constructQubitName var.Expression else "UNKNOWN")
        |> Seq.where (fun x -> not(x.Equals("UNKNOWN"))) |> Seq.toList
    | _ -> ["UNKNOWN"]


/// Returns the parameter list (parametric gates) as an array of strings
let getParametricVars (expr: QsExpressionKind<TypedExpression, Identifier, ResolvedType>) =
    let getClassicalVar (expr: QsExpressionKind<TypedExpression, Identifier, ResolvedType>) =
        match expr with
        | Identifier (LocalVariable varName, _) ->  Some( { typeName = "string"; value =  varName.Value } )
        | DoubleLiteral paramDoubleVal -> Some( { typeName = "double"; value =  paramDoubleVal.ToString() } )
        | _ -> None

    match expr with
    | ValueTuple (varTuple) -> 
        varTuple |> Seq.map (function var -> if not(containsQubit var.ResolvedType.Resolution) then getClassicalVar var.Expression else None )
        |> Seq.where (fun x -> x.IsSome) |> Seq.map (fun x -> x.Value) |> Seq.toList
    | _ -> []


/// Returns a GateCall corresponding to the given Expression
let fromExpression (expr: QsExpressionKind<TypedExpression, Identifier, ResolvedType>) =
    match expr with
    | CallLikeExpression  (expr1, expr2) -> Some({ gate = getGateName expr1.Expression;  qubits = getQubitVars expr2.Expression; gateParams = getParametricVars expr2.Expression })
    | _ -> printfn "Cannot convert to GateCall"; None

let parseForExpression (stmt: QsStatement) =
    let getStatementExpression (stmt : QsStatementKind) = 
        match stmt with 
        | QsExpressionStatement (exprStmt) -> fromExpression exprStmt.Expression
        | _ -> None

    match stmt.Statement with
    | QsForStatement forStmt -> Some({
        beginIdx = 0;
        endIdx = 0;
        comparator = ">";
        varName = "i";
        inc_or_dec = "++";
        body = forStmt.Body.Statements |> Seq.map (function x -> getStatementExpression x.Statement) |> Seq.map (function x -> x.Value) |> Seq.toList })
    | _ -> None


let parseQubitScopeBlock (scopeBlock: QsQubitScope) =
    let typeStr = scopeBlock.Kind.ToString()

    let getScopeArgs (lhsArg : SymbolTuple) = 
        match lhsArg with 
        | VariableName varName -> [varName.Value]
        | _ -> []

    let lhsVars = getScopeArgs scopeBlock.Binding.Lhs

    let getScopeAssignments (rhsArg : ResolvedInitializer) = 
        let getQubitRegSize (allocExpr : QsExpressionKind<TypedExpression, Identifier, ResolvedType>) = 
            match allocExpr with 
            | IntLiteral len -> len
            | _ -> (int64)(-1)
        match rhsArg.Resolution with 
        | SingleQubitAllocation -> [ "Qubit" ]
        | QubitRegisterAllocation regAlloc -> [String.Concat("Qubit", "[", getQubitRegSize regAlloc.Expression, "]")]
        | _ -> []


    let rhsAssign = getScopeAssignments scopeBlock.Binding.Rhs
    // Returns:
    (typeStr, lhsVars, rhsAssign)

/// Try to resolve a call into standard lib
/// 
/// Returns a list of resolved expression
let resolveLibCallExpression (callable: SpecializationImplementation, callExpr: QsExpressionKind<TypedExpression, Identifier, ResolvedType>) =
    let getCallableName (callExpr: QsExpressionKind<TypedExpression, Identifier, ResolvedType>) = 
        match callExpr with 
        | Identifier (GlobalCallable qualName, _) ->  qualName.Name.Value 
        | _ -> ""

    let getCallableArgs (argsExpr: QsExpressionKind<TypedExpression, Identifier, ResolvedType>) = 
        let getArgName (callExpr: QsExpressionKind<TypedExpression, Identifier, ResolvedType>) = 
            match callExpr with 
            | Identifier (qualName, _) ->  
                match qualName with 
                | GlobalCallable globalName -> globalName.Name.Value 
                | LocalVariable localName -> localName.Value
                | _ -> "__INVALID"
            | _ -> ""

        match argsExpr with 
        // TODO: we need to expand this more as we go to handle more cases
        | ValueTuple (varTuple) -> varTuple |> Seq.map (function x -> getArgName x.Expression) |> Seq.toList
        | _ -> []

    let getExpectedVars (callableImpl: SpecializationImplementation) = 
        let getVarTuple (varTup: QsTuple<LocalVariableDeclaration<QsLocalSymbol>>) = 
            let getVarName (varName: LocalVariableDeclaration<QsLocalSymbol>) = 
                match varName.VariableName with 
                | ValidName x -> x.Value
                | _ -> "__INVALID"

            let getVarNameInArray (tupItems: QsTuple<LocalVariableDeclaration<QsLocalSymbol>>) =
                match tupItems with 
                | QsTupleItem item -> getVarName item
                // Don't support nested tuples atm for simplicity
                | QsTuple tupleItems -> printfn "Unsupported!!!"; ""

            match varTup with 
            | QsTupleItem item -> [getVarName item]
            | QsTuple tupleItems -> tupleItems |> Seq.map (function x -> getVarNameInArray x) |> Seq.toList

        match callableImpl with 
        | Provided (varTuple, body) -> getVarTuple varTuple
        | _ -> printfn "Invalid callable expression"; []
    // Returns a pair of (var lists) and the resolved args.
    match callExpr with
    | CallLikeExpression  (expr1, expr2) -> (getExpectedVars callable, getCallableArgs expr2.Expression)
        // printfn "Callable Signature: %A \n Callable Name: %s \n Callable Args: %A"  (getExpectedVars callable) (getCallableName expr1.Expression) (getCallableArgs expr2.Expression)
    | _ -> printfn "Invalid callable expression"; ([],[])
   

