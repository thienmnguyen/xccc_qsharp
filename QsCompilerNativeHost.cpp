#include "QsCompilerNativeHost.hpp"
#include <iostream>
#include <dlfcn.h>
#include <limits.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

// Dotnet interop headers
#include <nethost.h>
#include "hostfxr.h"


namespace {
    // Dotnet Host FXR Function pointer
    hostfxr_initialize_for_runtime_config_fn g_init_fptr;
    hostfxr_get_runtime_delegate_fn g_get_delegate_fptr;
    hostfxr_close_fn g_close_fptr;

    void* loadLibrary(const char_t* in_path)
    {
        void* handle = dlopen(in_path, RTLD_LAZY | RTLD_LOCAL);
        assert(handle != nullptr);
        return handle;
    }

    void* getExport(void* handle, const char* name)
    {
        void* f = dlsym(handle, name);
        assert(f != nullptr);
        return f;
    }


    bool loadHostfxr()
    {
        // Pre-allocate a large buffer for the path to hostfxr
        char_t buffer[PATH_MAX];
        size_t buffer_size = sizeof(buffer) / sizeof(char_t);
        int rc = get_hostfxr_path(buffer, &buffer_size, nullptr);
        if (rc != 0)
        {
            return false;
        }            

        // Load hostfxr and get desired exports
        void* lib = loadLibrary(buffer);
        g_init_fptr = (hostfxr_initialize_for_runtime_config_fn)getExport(lib, "hostfxr_initialize_for_runtime_config");
        g_get_delegate_fptr = (hostfxr_get_runtime_delegate_fn)getExport(lib, "hostfxr_get_runtime_delegate");
        g_close_fptr = (hostfxr_close_fn)getExport(lib, "hostfxr_close");

        return (g_init_fptr && g_get_delegate_fptr && g_close_fptr);
    }

    load_assembly_and_get_function_pointer_fn get_dotnet_load_assembly(const char_t* assemblyPath)
    {
        // Load .NET Core
        void* loadAssemblyAndGetFnPtr = nullptr;
        hostfxr_handle cxt = nullptr;
        int rc = g_init_fptr(assemblyPath, nullptr, &cxt);
        if (rc != 0 || cxt == nullptr)
        {
            std::cerr << "Init failed: " << std::hex << std::showbase << rc << std::endl;
            g_close_fptr(cxt);
            return nullptr;
        }

        // Get the load assembly function pointer
        rc = g_get_delegate_fptr(
            cxt,
            hdt_load_assembly_and_get_function_pointer,
            &loadAssemblyAndGetFnPtr);
        
        if (rc != 0 || loadAssemblyAndGetFnPtr == nullptr)
        {
            std::cerr << "Get delegate failed: " << std::hex << std::showbase << rc << std::endl;
        }

        g_close_fptr(cxt);
        return (load_assembly_and_get_function_pointer_fn)loadAssemblyAndGetFnPtr;
    } 
}

namespace xacc {
namespace Interop{

QsCompilerHost::QsCompilerHost(SyntaxListener& io_syntaxListener, ErrorListener& io_errorListenter):
    m_syntaxListener(io_syntaxListener),
    m_errorListener(io_errorListenter),
    m_isInitialized(false),
    m_compileFnPtr(nullptr)
{}
bool QsCompilerHost::Initialize(const std::string& in_dllRootPath)
{
    //
    // STEP 1: Load HostFxr and get exported hosting functions
    //
    if (!loadHostfxr())
    {
        assert(false && "Failure: load_hostfxr()");
        return false;
    }

    //
    // STEP 2: Initialize and start the .NET Core runtime
    //
    const std::string configPath = in_dllRootPath + "XACC_Adapter.runtimeconfig.json";
    load_assembly_and_get_function_pointer_fn loadAssemblyAndGetFnPtr = nullptr;
    loadAssemblyAndGetFnPtr = get_dotnet_load_assembly(configPath.c_str());
    
    if (loadAssemblyAndGetFnPtr == nullptr)
    {
        std::cerr << "get_dotnet_load_assembly failed" << std::endl;
        return false;
    }
    

    //
    // STEP 3: Load managed assembly and get function pointer to a managed method
    //
    const std::string dotnetlibPath = in_dllRootPath + "XACC_Adapter.dll";
    const char_t* dotnetType = "XACC_Adapter.CompilerAdapter, XACC_Adapter";
    
    {
        const char_t* dotnetTypeMethod = "Compile";
        
        int rc = loadAssemblyAndGetFnPtr(
            dotnetlibPath.c_str(),
            dotnetType,
            dotnetTypeMethod,
            nullptr /*delegate_type_name*/,
            nullptr,
            (void**)&m_compileFnPtr);
        
        if (rc != 0 || m_compileFnPtr == nullptr)
        {
            std::cerr << "loadAssemblyAndGetFnPtr to method '" << dotnetTypeMethod << "' failed: " << std::hex << std::showbase << rc << std::endl;
            return false;
        }
    }
    
    {
        const char_t* dotnetTypeMethod = "GetNodeType";
        
        int rc = loadAssemblyAndGetFnPtr(
            dotnetlibPath.c_str(),
            dotnetType,
            dotnetTypeMethod,
            nullptr /*delegate_type_name*/,
            nullptr,
            (void**)&m_nodeInfoQueryFnPtr);
        
        if (rc != 0 || m_nodeInfoQueryFnPtr == nullptr)
        {
            std::cerr << "loadAssemblyAndGetFnPtr to method '" << dotnetTypeMethod << "' failed: " << std::hex << std::showbase << rc << std::endl;
            return false;
        }
    }

    {
        const char_t* dotnetTypeMethod = "VisitChildren";
        
        int rc = loadAssemblyAndGetFnPtr(
            dotnetlibPath.c_str(),
            dotnetType,
            dotnetTypeMethod,
            nullptr /*delegate_type_name*/,
            nullptr,
            (void**)&m_visitNodeFnPtr);
        
        if (rc != 0 || m_visitNodeFnPtr == nullptr)
        {
            std::cerr << "loadAssemblyAndGetFnPtr to method '" << dotnetTypeMethod << "' failed: " << std::hex << std::showbase << rc << std::endl;
            return false;
        }
    }

    m_isInitialized = true;
    
    return true;
}
bool QsCompilerHost::Compile(const std::string& in_srcString)
{
    QsCompileArgs qsArgs
    {
        in_srcString.c_str(),
        reinterpret_cast<void *&>(m_syntaxListener),
        reinterpret_cast<void *&>(m_errorListener),
    };

    try
    {
        m_compileFnPtr(&qsArgs, sizeof(qsArgs));
    }
    catch(const std::exception& e)
    {
        std::cerr << "Exception caught: \n";
        std::cerr << e.what() << '\n';
        return false;
    }
    

    // Test only
    {
        QsNodeQueryArgs nodeCheckArgs
        {
            1234,
            nullptr
        };
        const auto nodeTypeAsInt = m_nodeInfoQueryFnPtr(&nodeCheckArgs, sizeof(nodeCheckArgs));
        m_visitNodeFnPtr(&nodeCheckArgs, sizeof(nodeCheckArgs));
    }

    return true;
}
}
} 