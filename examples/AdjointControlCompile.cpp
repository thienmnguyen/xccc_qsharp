#include "xacc.hpp"

int main (int argc, char** argv) {

	// Initialize the XACC Framework
	xacc::Initialize(argc, argv);

	auto qpu = xacc::getAccelerator("qpp", { std::make_pair("shots", 1000) });

	// Create a Program
	auto qsharpCompiler = xacc::getCompiler("qsharp");
    auto ir = qsharpCompiler->compile(R"(
namespace Quantum.XACC {
    open Microsoft.Quantum.Intrinsic;
    operation qFunc1(q1 : Qubit, q2 : Qubit) : Unit {
        Adjoint T(q1);
        SWAP(q1, q2);
        Controlled X(q1, q2);
    }
}
)", qpu);
    std::cout << "COMPILED: \n\n";

    for (const auto& composite : ir->getComposites())
    {
        std::cout << composite->name() << "\n";
        std::cout << composite->toString() << "\n\n";
    }
    // Get the composite
    auto program = ir->getComposite("Quantum.XACC.qFunc1");
    // Run simulation 
    auto buffer = xacc::qalloc(2);
    qpu->execute(buffer, program);
    // Expected: entangled state: 50-50 (00 and 11)
    buffer->print();
    // Finalize the XACC Framework
    xacc::Finalize();

    return 0;
}