#include "xacc.hpp"
#include "Optimizer.hpp"
#include "xacc_observable.hpp"
#include "xacc_service.hpp"

int main (int argc, char** argv) {

	// Initialize the XACC Framework
	xacc::Initialize(argc, argv);

	auto qpu = xacc::getAccelerator("qpp");
    // Create the N=2 deuteron Hamiltonian
    auto H_N_2 = xacc::quantum::getObservable(
        "pauli", std::string("5.907 - 2.1433 X0X1 "
                           "- 2.1433 Y0Y1"
                           "+ .21829 Z0 - 6.125 Z1"));

    auto optimizer = xacc::getOptimizer("nlopt");

	// Create a Q# Program (Deuteuron H2 ansatz)
	auto qsharpCompiler = xacc::getCompiler("qsharp");
    auto ir = qsharpCompiler->compile(R"(
namespace Quantum.XACC {
    open Microsoft.Quantum.Intrinsic;
    operation AnsatzH2(qReg : Qubit[], theta : Double) : Unit {
        X(qReg[0]);
        Ry(qReg[1], theta);
        CNOT(qReg[1], qReg[0]);
        M(qReg[0]);
        M(qReg[1]);
    }
}
)", qpu);
    std::cout << "COMPILED: \n\n";

    for (const auto& composite : ir->getComposites())
    {
        std::cout << composite->name() << "\n";
        std::cout << composite->toString() << "\n\n";
    }
    // Get the composite (full namespace-qualified name)
    auto program = ir->getComposite("Quantum.XACC.AnsatzH2");
    // Get the VQE Algorithm and initialize it
    auto vqe = xacc::getAlgorithm("vqe");
    vqe->initialize({std::make_pair("ansatz", program),
                    std::make_pair("observable", H_N_2),
                    std::make_pair("accelerator", qpu),
                    std::make_pair("optimizer", optimizer)});

    // Allocate some qubits and execute
    auto buffer = xacc::qalloc(2);
    vqe->execute(buffer);

    // Print the result
    std::cout << "Energy: " << (*buffer)["opt-val"].as<double>() << "\n";
    
    // Finalize the XACC Framework
    xacc::Finalize();

	return 0;
}