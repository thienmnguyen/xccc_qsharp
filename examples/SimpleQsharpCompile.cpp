#include "xacc.hpp"

int main (int argc, char** argv) {

	// Initialize the XACC Framework
	xacc::Initialize(argc, argv);

	auto qpu = xacc::getAccelerator("qpp", { std::make_pair("shots", 1000) });

	// Create a Program
	auto qsharpCompiler = xacc::getCompiler("qsharp");
    auto ir = qsharpCompiler->compile(R"(
namespace Quantum.XACC {
    open Microsoft.Quantum.Intrinsic;
    operation qFunc1(q1 : Qubit, q2 : Qubit) : Unit {
        X(q1);
        H(q1);
        CNOT(q1, q2);
        M(q1);
        M(q2);
    }

    operation qFunc2(qReg : Qubit[], theta : Double) : Unit {
        Rx(qReg[1], theta);
        M(qReg[0]);
        H(qReg[0]);
        Ry(qReg[1], 3.1415);
        CNOT(qReg[1], qReg[0]);
        M(qReg);
    }
}
)", qpu);
    std::cout << "COMPILED: \n\n";

    for (const auto& composite : ir->getComposites())
    {
        std::cout << composite->name() << "\n";
        std::cout << composite->toString() << "\n\n";
    }
    // Get the composite
    auto program = ir->getComposite("Quantum.XACC.qFunc1");
    // Run simulation 
    auto buffer = xacc::qalloc(2);
    qpu->execute(buffer, program);
    // Expected: entangled state: 50-50 (00 and 11)
    buffer->print();
    // Finalize the XACC Framework
    xacc::Finalize();

    return 0;
}