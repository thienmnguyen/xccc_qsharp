import sys
from pathlib import Path
sys.path.insert(1, str(Path.home()) + '/.xacc')
import xacc

# Get access to the desired QPU and
# allocate some qubits to run on
qpu = xacc.getAccelerator('qpp')
buffer = xacc.qalloc(2)

# Construct the Hamiltonian as an XACC-VQE PauliOperator
ham = xacc.getObservable('pauli', '5.907 - 2.1433 X0X1 - 2.1433 Y0Y1 + .21829 Z0 - 6.125 Z1')

# Define the ansatz 
# Get the Q# compiler
qsCompiler = xacc.getCompiler('qsharp')
# Deuteron Ansatz circuit in Q#
ir = qsCompiler.compile('''
namespace Quantum.XACC {
    open Microsoft.Quantum.Intrinsic;
    operation AnsatzH2(qReg : Qubit[], theta : Double) : Unit {
        X(qReg[0]);
        Ry(qReg[1], theta);
        CNOT(qReg[1], qReg[0]);
    }
}''', qpu)

program = ir.getComposites()[0]
opt = xacc.getOptimizer('nlopt')

# Create the VQE algorithm
vqe = xacc.getAlgorithm('vqe', {
                                'ansatz': program,
                                'accelerator': qpu,
                                'observable': ham,
                                'optimizer': opt
                            })
vqe.execute(buffer)
print(buffer)
print(f'>> Energy: {buffer["opt-val"]:.5f} <<')