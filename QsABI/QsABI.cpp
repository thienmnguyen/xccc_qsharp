#include "QsABI.h"
#include <iostream>

namespace {
    std::string qsNodeTypeToString(QS_AST_NODE_TYPE nodeType)
    {
        switch (nodeType)
        {
        case QS_AST_NODE_TYPE::QS_EXPRESSION:
            return "Expression";   
        case QS_AST_NODE_TYPE::QS_STATEMENT:
            return "Statement";            
        case QS_AST_NODE_TYPE::QS_INVALID:
            return "Invalid";
        default:
            return "";
        }
    }
}

void OnCompilationCompleted(void* listener, QsCompilation* compilationResult)
{
    SyntaxListener* listenerCast = (SyntaxListener*)listener;
    listenerCast->log(*compilationResult);    
}

// Hook to be called when an Intrinsic quantum gate is visited.
// This is called during initial AST tree discovery.
// Max 2-qubit
void OnIntrinsicGateVisited(void* listener, const char* gateName, int arity, const char* q1, const char* q2, int nbVars, QsParametricVar* paramArray)
{
    std::vector<GateVariable> vars;
    for (int i = 0; i < nbVars; ++i)
    {
        vars.emplace_back(paramArray[i]);
    }
    
    SyntaxListener* listenerCast = reinterpret_cast<SyntaxListener*>(listener);
    if (arity == 1)
    {
        return listenerCast->visitGate(gateName, { q1 }, vars);
    }
    if (arity == 2)
    {
        return listenerCast->visitGate(gateName, { q1, q2 }, vars);
    }
    std::cout << "ERROR! Unsupported GATE encountered!\n";
}

void OnKernelVisited(void* listener, const char* kernelName, int nbVars, QsKernelVar* varArray)
{
    std::vector<KernelVariable> vars;
    for (int i = 0; i < nbVars; ++i)
    {
        vars.emplace_back((varArray)[i]);
    }

    SyntaxListener* listenerCast = (SyntaxListener*)listener;
    listenerCast->visitKernel(kernelName, vars);
}

QS_AST_VISIT_RESULT VisitNode(int nodeId, QS_AST_NODE_TYPE nodeType, int parentNodeId, void* clientData)
{
    std::cout << "Visit node Id " << nodeId << " of type " << qsNodeTypeToString(nodeType) << "\n";
}


void SyntaxListener::log(const QsCompilation& in_compilationResult)
{
    std::cout << "There are " << in_compilationResult.nbNamespaces << " namespaces in the code.\n";

    for (size_t i = 0; i < in_compilationResult.nbNamespaces; ++i)
    {
        const auto nsNode = in_compilationResult.namespaceArray[i];
        std::cout << "Visit namespace '" << nsNode.name << "' node.\n";
    }
}


void SyntaxListener::visitGate(const std::string& in_gateName, const std::vector<std::string>& in_qubits, const std::vector<GateVariable>& in_gateVars)
{
    std::cout << "Gate: " << in_gateName << " ";
    for (const auto& qubit : in_qubits)
    {
        std::cout << qubit << " ";
    }

    for (const auto& parVar : in_gateVars)
    {
        std::cout << parVar.toString() << " ";
    }

    std::cout << "\n";
    if (g_qsAstTreeWalker)
    {
        g_qsAstTreeWalker->enterGate(in_gateName, in_qubits, in_gateVars);
    }
}

void SyntaxListener::visitKernel(const std::string& in_kernelName, const std::vector<KernelVariable>& in_vars)
{
    std::cout << "Kernel: " << in_kernelName << "\n";
    for (const auto& var : in_vars)
    {
        std::cout << "Var name: " << var.m_name << " (" << var.m_type << ")\n";
    }
    if (g_qsAstTreeWalker)
    {
        g_qsAstTreeWalker->enterKernel(in_kernelName, in_vars);
    }
}
