#pragma once

#include <string>
#include <vector>
#include <optional>

typedef struct _QsNamespace
{
    char* name;
} QsNamespace;

typedef struct _QsCompilation
{
    int nbNamespaces;
    QsNamespace* namespaceArray;
} QsCompilation;


typedef struct _QsKernelVar
{
    char* name;
    char* type;
} QsKernelVar;

typedef struct _QsParametricVar
{
    bool isVar;
    // If isVar = true, contains variable name
    char* varName;
    // If isVar = false, this is the value
    double value;
} QsParametricVar;

struct KernelVariable
{
    KernelVariable(const QsKernelVar& in_var):
        m_name(in_var.name),
        m_type(in_var.type)
    {}

    std::string m_name;
    std::string m_type;
};

struct GateVariable
{
    GateVariable(const QsParametricVar& in_param):
        m_name(in_param.isVar ? in_param.varName : ""),
        m_val(in_param.isVar ? 0.0 : in_param.value)
    {}

    std::optional<double> getValue() const
    {
        return m_name.empty() ? m_val : std::optional<double>();
    } 

    std::optional<std::string> getName() const
    {
        return m_name.empty() ? std::optional<std::string>() : m_name;
    } 

    bool isNamedVariable() const { return getName().has_value();}

    std::string toString() const { return isNamedVariable() ? m_name : std::to_string(m_val);}
private:
    std::string m_name;
    double m_val;
};

enum class QS_AST_NODE_TYPE
{
    QS_INVALID,
    QS_STATEMENT,
    QS_EXPRESSION
};

enum class QS_AST_VISIT_RESULT
{
    VISIT_BREAK,
    VISIT_CONTINUE,
    VISIT_RECURSE
};


extern "C" 
{
    void OnCompilationCompleted(void* listener, QsCompilation* compilationResult);
    
    void OnIntrinsicGateVisited(void* listener, const char* gateName, int arity, const char* q1, const char* q2, int nbVars, QsParametricVar* paramArray);
    
    // QsCallable => XACC kernel
    void OnKernelVisited(void* listener, const char* kernelName, int nbVars, QsKernelVar* varArray);

    QS_AST_VISIT_RESULT VisitNode(int nodeId, QS_AST_NODE_TYPE nodeType, int parentNodeId, void* clientData);
}

struct ITreeWalker
{
    virtual void enterGate(const std::string& in_gateName, const std::vector<std::string>& in_qubits, const std::vector<GateVariable>& in_gateVars) = 0;
    virtual void enterKernel(const std::string& in_kernelName, const std::vector<KernelVariable>& in_vars) = 0;
};

extern ITreeWalker* g_qsAstTreeWalker;

class SyntaxListener
{
public:
    void log(const QsCompilation& in_compilationResult);
    void visitGate(const std::string& in_gateName, const std::vector<std::string>& in_qubits, const std::vector<GateVariable>& in_gateVars);
    void visitKernel(const std::string& in_kernelName, const std::vector<KernelVariable>& in_vars);
};

class ErrorListener
{
public:
    // TODO:
    void onError() {};
};